<?php

namespace App\Http\Controllers;

use App\Randomizer;
use App\RandomItems;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    // SHOW HOME PAGE
    public function index()
    {
        if(Auth::check() && Auth::user()->admin) {
            $data = Randomizer::ID()->orderBy('id', 'desc')->with('items')->paginate(10);
        } else {
            // get all records where public is 1 or 0 and created by the logged in user and order it from new to old, after 10 records create a new page
            $data = Randomizer::ID()->where('public', 1)->orWhere([['public', '=', 0], ['user_id', '=', Auth::id()]])->orderBy('id', 'desc')->paginate(10);
        }
        // view the home file and send the variable data to that file
        return view('home', compact('data'));
    }

    public function search(Request $req)
    {
        if($req->ajax()){
            $output = '';
            $title = $req->get('title');
            if($title != ''){
                // search db records
                if(Auth::check() && Auth::user()->admin) {
                    $results = Randomizer::ID()->where('title', "LIKE", "%".$req->title."%")->orderBy('id', 'desc')->get();
                } else {
                    $results = Randomizer::where('title', "LIKE", "%".$req->title."%")->where('public', 1)->orWhere([['title', "LIKE", "%".$req->title."%"], ['public', '=', 0], ['user_id', '=', Auth::id()]])->orderBy('id', 'desc')->get();
                }
            } else {
                $results = [
                    0 => ['title' => 'Please fill in a keyword!']
                ];
            }
            // return response()->json([
            //     'results' => view('layouts.results', compact('results'))
            // ]);

            return response()->json($results);
        }

    }
}
