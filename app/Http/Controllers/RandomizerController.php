<?php

namespace App\Http\Controllers;

use App\Randomizer;
use App\RandomItems;
use App\User;
use Illuminate\Http\Request;
use Auth;

class RandomizerController extends Controller
{

    // SHOW RANDOMIZER WITH CERTAIN ID
    public function index($id)
    {
        // Find randomizer with a given id or fail
    	$randomshow = Randomizer::with('items')->findOrFail($id);
        // shows the randomizer if it is public or the creator or the admin goes to the page
        if($randomshow->public || $randomshow->public === 0 && $randomshow->user_id === Auth::id() || Auth::user()->admin) {
            // get show.blade.php in the randomizer folder and send the randomshow variable
            return view('randomizer.show', compact('randomshow'));
        } else {
            // if you are not authorized to see the page send 403 error
            abort(403, 'Unauthorized action.');
        }
    }

    // SHOW ALL RANDOMIZERS CREATED BY SPECIFIC USER
    public function userRandomizers($user_id) {
        $user = User::findOrFail($user_id);
        if(Auth::check() && Auth::user()->admin) {
            $randomizers = Randomizer::where('user_id', $user_id)->paginate(10);
        } else {
            $randomizers = Randomizer::where([['user_id', '=', $user_id], ['public', '=', 1]])->paginate(10);
        }

        return view('profile.userRandomizers', compact('randomizers', 'user'));
    }

    // SHOW RANDOMIZER FORM
    public function addRandomizer()
    {
        // get add.blade.php in the randomizer folder
    	return view('randomizer.add');
    }

    // STORE RANDOMIZER
    public function storeRandomizer(Request $request)
    {
        // get this user id
        $userId = Auth::id();
        // get all form fields
        $input = $request->all();

        // validate the fields with the requirements
        $this->validate($request, [
            'title' => 'required|max:255|unique:randomizer',
            'item.*' => 'required',
            'public' => 'boolean',
        ]);

        // create a new database record for randomizer
        $randomizer = new Randomizer;

        // retrieve model by its primary key
        $randomTitle = Randomizer::find(1);

        // get value of input named title and set it to the title of randomizer
        $randomizer->title = $request->get('title');

        // get value of input named public and set it to the public column of randomizer
        $randomizer->public = $request->get('public');

        // set the user id to the user_id column of randomizer
        $randomizer->user_id = $userId;

        // checks if the checkbox is checked
        if(!$randomizer->public) {

            // if not, set it to 0
            $randomizer->public = 0;
        }

        // save the randomizer to the database
        $randomizer->save();

        // for every input named item run this loop
        for ($i=0; $i < count($input['item']); ++$i) 
        {
            // create a new database record for randomitems
            $randomItems = new RandomItems;

            // get value of input named item from the array and set it to the item column of randomitems
            $randomItems->item = $input['item'][$i];

            // set the relationship randomizer id to the randomizer_id column of randomitems
            $randomItems->randomizer()->associate($randomizer);

            // save the randomitems to the database
            $randomItems->save();
        }
        
        // go back to the form
        return redirect('/randomizer/add');
    }

    public function editRandomizer($id) {

        $randomshow = Randomizer::with('items')->findOrFail($id);

        if($randomshow->user_id === Auth::id() || Auth::user()->admin) {
            // get show.blade.php in the randomizer folder and send the randomshow variable
            return view('randomizer.update', compact('randomshow'));
        } else {
            // if you are not authorized to see the page send 403 error
            abort(403, 'Unauthorized action.');
        }
    }

    public function updateRandomizer($id, Request $request){
        $randomizer = Randomizer::findOrFail($id);
        $items = RandomItems::all()->where('randomizer_id', $id);
        $input = $request->all();

        $this->validate($request, [
            'title' => 'required|max:255',
            'item.*' => 'required',
            'public' => 'boolean',
        ]);

        // update randomizer record
        $randomizer->title = $input['title'];
        $randomizer->public = $request->get('public');
        // checks if the checkbox is checked
        if(!$randomizer->public) {

            // if not, set it to 0
            $randomizer->public = 0;
        }
        $randomizer->save();

        // update randomitems records
        $key = 0;
        foreach($input['item'] as $item) {
            $i = $items->keys()[$key];

            $items[$i]->updateOrCreate(
                ['randomizer_id' => $id, 'item' => $items[$i]->item ],
                ['item' => $item]
            );

            if($key < (count($items->keys())-1)) {
                $key++;
            }
        }

        return redirect('/randomizer/update/'.$id);
    }

    public function deleteItem(Request $req){
        RandomItems::find($req->id)->delete();
        return response()->json();
    }

    public function deleteRandomizer($id){
        Randomizer::findOrFail($id)->delete();
        return redirect()->route('profile.randomizers');
    }
}
