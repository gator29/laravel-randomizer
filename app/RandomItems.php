<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RandomItems extends Model
{
	// set table name
    protected $table = 'randomitems';

    // set date columns
    protected $dates = ['created_at', 'updated_at'];
    protected $guarded = [];

    // set fillable columns
	protected $fillable = [
        'item', 'randomizer_id',
    ];

    // create relationship function
    public function randomizer() {
    	return $this->belongsTo('App\Randomizer');
    }
}
