<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Randomizer extends Model
{
    // set table name
    protected $table = 'randomizer';

    // set the date columns
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $guarded = [];

    // set the fillable columns
	protected $fillable = [
        'title', 'public',
    ];

    // create relationship function
    public function items() {
    	return $this->hasMany('App\RandomItems');
    }

    public function user() {
    	return $this->belongsTo('App\User');
    }

    // get all ids in the table
    public function scopeID($query){
        return $query->where('id', '>', '0');
    }
}
