$(document).ready(function(){
	$(document).on('click', '.delete-modal', function(){
        $('#footer_action_button').removeClass('glyphicon-check');
        $('#footer_action_button').addClass('glyphicon-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete');
        $('.did').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.dname').html($(this).data('name'));
        $('#myModal').modal('show');
	});

	$('.modal-footer').on('click', '.delete', function(){
		$.ajax({
			type: 'post',
			url: '/randomizer/delete/item',
			data: {
				'_token': $('input[name=_token]').val(),
				'id': $('.did').text()
			},
			success: function(data){
				$('.item' + $('.did').text()).remove();
			}
		});
	});

	$('.del-randomizer').on("submit", function() {
		return confirm("Are you sure you want to delete this randomizer? You can never restore it!");
	});
});