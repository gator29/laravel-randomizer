$(document).ready(function(){

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('.search-form').on('keyup click', '.search-box', function(){
		$.ajax({
			type: 'get',
			url: '/randomizer/search',
			data: {
				'title': $('.search-box').val()
			},
			dataType: 'json',
			success: function(data){
				var result = 0;
				$('.search-results').css('display', 'block');
				$('.search-results').empty();
				console.log(data);
				if(data.length != 0) {
					$.each(data, function()	{
						// IF NO ID, DO SOMETHING
						if(data[result].hasOwnProperty('id')) {
							$('.search-results').append('<li><a href="/randomizer/show/'+data[result].id+'">'+ data[result].title +'</a></li>');
						} else if(!data[result].hasOwnProperty('id')) {
							$('.search-results').append('<p>'+ data[result].title +'</p>');
						}
						result++;
					});
				} else {
					$('.search-results').append('<p>There are no search results</p>');
				}
				
			}
		});
	});
	$('.search-box').focusout(function() {
		$('body').click(function(event) {
			var target = $(event.target);
			if(!target.is('.search-results p')) {
				$('.search-results').removeAttr('style').empty();
			}
		});
	});
});