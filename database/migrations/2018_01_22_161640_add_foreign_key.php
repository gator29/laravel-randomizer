<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('randomitems', function($table) {
            $table->integer('randomizer_id')->unsigned()->change();
            $table->foreign('randomizer_id')
                ->references('id')->on('randomizer')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('randomitems', function($table) {
            $table->dropForeign(['randomizer_id']);
        });
    }
}
