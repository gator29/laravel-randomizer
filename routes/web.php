<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// LOGIN AND REGISTRATION
Auth::routes();

// HOME PAGE
Route::get('/', ['as' => '/', 'uses' => 'HomeController@index']);

// SEARCH ROUTE
Route::get('/randomizer/search', 'HomeController@search');

// RANDOMIZER PAGES
// routes to create randomizers
Route::get('randomizer/add', ['as' => 'randomizer.add', 'uses' => 'RandomizerController@addRandomizer'])->middleware('auth');
Route::post('randomizer/add', 'RandomizerController@storeRandomizer')->middleware('auth');

// routes to show the randomizer
Route::get('randomizer/show/{id}', ['as' => 'randomizer.show', 'uses' => 'RandomizerController@index']);

// routes to update/edit randomizers
Route::get('randomizer/update/{id}', ['as' => 'randomizer.update', 'uses' => 'RandomizerController@editRandomizer'])->middleware('auth');
Route::patch('randomizer/update/{id}', ['as' => 'randomizer.update', 'uses' => 'RandomizerController@updateRandomizer'])->middleware('auth');

// routes to delete randomizer and items
Route::post('randomizer/delete/item', 'RandomizerController@deleteItem')->middleware('auth');

// routes to randomizers created by logged in user
Route::get('profile/randomizers', ['as' => 'profile.randomizers', 'uses' => 'UserController@randomizers'])->middleware('auth');

// routes to randomizers created by specific user
Route::get('profile/{user_id}/randomizers', ['as' => 'profile.userRandomizers', 'uses' => 'RandomizerController@userRandomizers']);

// routes to delete randomizers
Route::delete('randomizer/delete/{id}', ['as' => 'randomizer.delete', 'uses' => 'RandomizerController@deleteRandomizer'])->middleware('auth');

// PROFILE PAGES
Route::get('profile', 'UserController@profile')->middleware('auth');
Route::post('profile', 'UserController@update_avatar')->middleware('auth');