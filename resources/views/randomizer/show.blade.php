@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{!! html_entity_decode($randomshow->title) !!}</div>

                <div class="panel-body">
                    <!-- STORE ALL ITEMS IN AN ARRAY -->
                @php
                    $all_arrays = [];
                    foreach($randomshow->items as $item){
                        array_push($all_arrays, $item->item);
                    }
                @endphp
                <span id="random_output">
                    <script type="text/javascript">
                        // SEND THE ARRAY TO THE JS FUNCTION TO GET A RANDOM ITEM
                        randomize(<?php echo json_encode($all_arrays); ?>)
                    </script>
                </span>
                <span>
                    <button type="button" class="btn btn-secondary btn-sm" onclick='randomize(<?php echo json_encode($all_arrays, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE); ?>)'>Randomize!</button>
                </span>
                <!-- {!! html_entity_decode($item->item) !!} -->
                    <a href="{{ route('randomizer.update', $randomshow->id) }}">UPDATE</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection