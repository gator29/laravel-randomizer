@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Formulier</div>

                <div class="panel-body">
                    <!-- Form -->
                    <form  action="/randomizer/add" method="POST">
                        <div class="form-group">
                            <input type="text" name="title" class="form-control" placeholder="Title">
                        </div>
                        <div class="form-group" id="paste">
                            <div class="input-group control-group after-add-more">
                                <input type="text" name="item[]" class="form-control" placeholder="Item">
                                <div class="input-group-btn"> 
                                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                                </div>
                            </div>
                        </div>
                        <label for="">Public</label>
                        <input type="checkbox" name="public" value="1"><br>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" value="submit" class="pull-right btn btn-sm btn-primary">
                    </form>

                    <!-- Copy Fields -->
                    <div class="copy hide">
                        <div class="control-group input-group" style="margin-top:10px">
                            <input type="text" name="item[]" class="form-control" placeholder="Item">
                            <div class="input-group-btn"> 
                                <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection