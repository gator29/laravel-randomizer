@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit {!! html_entity_decode($randomshow->title) !!}</div>

                <div class="panel-body">
                <span>
                    <!-- Form -->
                    <form action="{{ route('randomizer.update', $randomshow->id) }}" method="POST">
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="form-group">
                            <input type="text" name="title" class="form-control" placeholder="Title" value="{!! html_entity_decode($randomshow->title) !!}">
                        </div>
                        <div class="form-group" id="paste">
                            @php
                                $first = true;
                                foreach($randomshow->items as $item){
                                    if($first){
                                        @endphp
                                        <div class="input-group control-group after-add-more">
                                            <input type="text" name="item[]" class="form-control" placeholder="Item" value="{!! html_entity_decode($item->item) !!}">
                                            <div class="input-group-btn"> 
                                                <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                                            </div>
                                        </div>
                                        @php
                                        $first = false;
                                    } else {
                                    @endphp
                                        <div class="control-group input-group item{{$item->id}}" style="margin-top:10px">
                                            <input type="text" name="item[]" class="form-control" placeholder="Item" value="{!! html_entity_decode($item->item) !!}">
                                            <div class="input-group-btn">
                                                <button class="btn btn-danger delete-modal" data-id="{{$item->id}}" data-name="{{$item->item}}" type="button"><i class="glyphicon glyphicon-trash"></i> Remove</button>
                                            </div>
                                        </div>
                                        @php
                                    }
                                }
                            @endphp
                        </div>
                        <label for="">Public</label>
                        <input type="checkbox" name="public" value="1" id="" @if($randomshow->public == 1) checked @endif><br>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" value="submit" class="pull-right btn btn-sm btn-primary">
                    </form>

                    <!-- Copy Fields -->
                    <div class="copy hide">
                        <div class="control-group input-group" style="margin-top:10px">
                            <input type="text" name="item[]" class="form-control" placeholder="Item">
                            <div class="input-group-btn"> 
                                <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-trash"></i> Remove</button>
                            </div>
                        </div>
                    </div>

                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" role="form">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="id">ID:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="fid" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="name">Name:</label>
                                            <div class="col-sm-10">
                                                <input type="name" class="form-control" id="n">
                                            </div>
                                        </div>
                                    </form>
                                    <div class="deleteContent">
                                        Are you sure you want to delete <span class="dname"></span>?<span
                                            class="hidden did"></span>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn actionBtn" data-dismiss="modal">
                                            <span id="footer_action_button" class='glyphicon'></span> Delete
                                        </button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                                            <span class='glyphicon glyphicon-remove'></span> Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </span>
                <!-- {!! html_entity_decode($item->item) !!} -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection