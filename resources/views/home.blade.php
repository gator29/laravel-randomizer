@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                    <!-- Plaatst alle opgehaalde randomizers -->
                    @forelse ($data as $r)
                        <!-- Maakt een linkje van de titel van het gerelateerde id -->
                        <a href="{{ route('randomizer.show', $r->id) }}"><h3>{!! html_entity_decode($r->title) !!}</h3></a>
                        <p>
                            <h5>
                                @php 
                                    $items = [];
                                    $totalItems = count($r->items);
                                    $shown = 5;
                                    for ($i=0; $i < $totalItems; ++$i) {
                                        if($i < $shown) {
                                            array_push($items, html_entity_decode($r->items[$i]["item"]));
                                        }
                                    }
                                    echo implode(', ', $items);
                                    
                                    if($totalItems > $shown) {
                                        echo '&hellip;';
                                    }
                                @endphp
                            </h5>
                        </p>
                        <p>
                            <h6>
                                Created by: 
                                @if(Auth::check() && $r->user_id == Auth::id())
                                    <a href="{{ url('/profile/randomizers') }}">{!! html_entity_decode($r->user->name) !!}</a>
                                @else
                                    <a href="{{ route('profile.userRandomizers', $r->user_id) }}">{!! html_entity_decode($r->user->name) !!}</a>
                                @endif
                            </h6>
                        </p>
                        <hr>
                    @empty
                        <!-- Als er niks geplaatst is, ziet de gebruiker de tekst hieronder -->
                        At this moment there isn't any randomizer uploaded, check back later.
                    @endforelse
                    <!-- Linkjes naar de volgende pagina -->
                    {!! $data->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
