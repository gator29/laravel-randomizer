@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{!! html_entity_decode($user->name) !!}'s randomizers</div>
{{-- CREATE TABLE WITH UPDATE AND DELETE BUTTON --}}
                <div class="panel-body">
                    <!-- Plaatst alle opgehaalde randomizers -->
                    @forelse ($randomizers as $r)
                        <p>
                            <!-- Maakt een linkje van de titel van het gerelateerde id -->
                            <a href="{{ route('randomizer.show', $r->id) }}"><h3>{!! html_entity_decode($r->title) !!}</h3></a>
                        </p>
                        <hr>
                    @empty
                        <!-- Als er niks geplaatst is, ziet de gebruiker de tekst hieronder -->
                        At this moment this user hasn't created any randomizers, check back later.
                    @endforelse
                    <!-- Linkjes naar de volgende pagina -->
                    {!! $randomizers->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
