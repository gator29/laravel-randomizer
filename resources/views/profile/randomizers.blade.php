@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Created randomizers</div>
{{-- CREATE TABLE WITH UPDATE AND DELETE BUTTON --}}
                <div class="panel-body">
                    <!-- Plaatst alle opgehaalde randomizers -->
                    @forelse ($data as $r)
                        <p>
                            <!-- Maakt een linkje van de titel van het gerelateerde id -->
                            <a href="{{ route('randomizer.show', $r->id) }}"><h3>{!! html_entity_decode($r->title) !!}</h3></a>
                            <form action="{{ route('randomizer.delete', ['id' => $r->id]) }}" method="post" class="del-randomizer">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <input type="submit" value="Delete randomizer" class="btn btn-danger">
                            </form>
                        </p>
                        <hr>
                    @empty
                        <!-- Als er niks geplaatst is, ziet de gebruiker de tekst hieronder -->
                        At this moment you haven't created any randomizer, you can make one <a href="{{ url('randomizer/add') }}">here</a>.
                    @endforelse
                    <!-- Linkjes naar de volgende pagina -->
                    {!! $data->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
