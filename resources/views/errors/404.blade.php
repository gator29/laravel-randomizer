@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Error 404</div>

                <div class="panel-body">
                    The page you are looking for does not exist!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection